---
title: "The Coronet: Prince &amp; Princess of Insulae Draconis"
subtitle: Meet our Prince and Princess
prevurl:
  - /prince/progress.html
  - /Coronet/index.html
---
<table class="table table-striped" style="align: center">
    <tbody>
      <tr>
        <td width="50%" style="text-align: right">
          <h2>Prince</h2>
          <strong>Ranulf Li Norreis</strong><br>
          <a href="mailto:prince@insulaedraconis.org" >prince@insulaedraconis.org</a>
        </td>
        <td width="50%" style="text-align: left"><h2>Princess</h2>
            <strong>Euphrosyne Eirinikina </strong><br>
          <a href="mailto:princess@insulaedraconis.org">princess@insulaedraconis.org</a> <br>
          </td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center">
          <img width="400" src="/coronet/images/Ranulf-Euphrosyne.jpg" class="rounded shadow m-3" alt="Prince and Princess of Insulae Draconis"><br>
        </td>
      </tr>
<br>
 <table width="500" cellspacing="0" cellpadding="2">
    <tbody>
      <tr>
        <td>His Highness is interested in all medieval/pre-modern martial arts, from all around the world (rattan combat, fencing, cut and thrust, wrestling, archery, jousting, etc.), stories, songs, and dances, and embracing the joy of time spent in a shared love of the past.</td>
        <td>Her Highness is interested in jewelry making, fiber and textile arts, calligraphy and illumination, equestrian activities, foods from all over the world, and all the other fun and exciting aspects of SCA activities.</td>
      </tr>
      <tr>
        <td>His Highness likes seeing courteous and noble behavior, new people, people having a good time and enjoying themselves in a shared environment, and hearing stories of great deeds.</td>
        <td>Her Highness likes bright and colorful décor at events, people having a good time, and seeing gentlefolk helping each other.</td>
      </tr>
      <tr>
        <td>
        His Highness would like to see events that are accessible to as broad a range of people as possible.
        </td>
        <td>
        Her Highness would like to have lots of events to attend and to meet new people.
        </td>
      </tr>
      <tr>
        <td>
         His Highness dislikes being unable to see and enjoy every activity at an event because of an overbooked schedule
        </td>
        <td>
         Her Highness dislikes stressful environments and gentlefolk displaying disrespectful manners to each other.
        </td>
      </tr>
      <tr>
        <th colspan="2" style="text-align: center"><i><b>Regarding food</b></i></th>
      </tr>
      <tr>
        <td>
         Please feed his Highness vegetables of all varieties, anything local, sparkling water, and meats of all types.
        </td>
        <td>
         Please feed her Highness meat, seafood, vegetables, rice, and all kinds of fruits. 
        </td>
      </tr>
      <tr>
        <td>
         Please don't feed his Highness anything spicier than a habenero pepper.
        </td>
        <td>
         Please don't feed her Highness dairy products without warning (lactose intolerance), raw onion, or rosewater.
        </td>
      </tr>
      <tr>
        <th colspan="2" style="text-align: center"><i><b>Royal Whim</b></i></th>
      </tr>
      <tr>
        <td>
         We would love to pet your dog.
        </td>
        <td>
         We would love to pet your dog. 
        </td>
      </tr>
      <tr>
        <td>
         Respect people's pronouns and identities.
        </td>
        <td>
         Respect people's pronouns and identities. 
        </td>
      </tr>

  </tbody>
  </table>
  


<br />

<br />
<div id="mainContent">

  <h2><a name="progress"></a>Progress of the Prince and Princess</h2>

  <p>
  Events at which their Highnesses will be in attendance are <a href="{% link events/index.html %}">marked with a Crown icon in the Principality Calendar</a>.
  </p>

  <br />

  <h2>What you need to know</h2>

  <p>The Prince and Princess of Insulae Draconis represent the King and Queen of Drachenwald in the Principality. As such ceremonial representatives of the Crown of Drachenwald, the Prince and Princess should be treated as any other royal couple in the Kingdom. In essence, be courteous and respectful of what the Prince and Princess are representing and of the fact that their time is not their own. Much of each event they attend will be spent officiating, being in meetings, or just listening to people's opinions. Read more about how to <a href="{% link coronet/behaviour-around-royal-couple.md %}">behave around the Prince &amp; Princess</a> including at Court.</p>

  <p>The Prince and Princess will hold Court at most events at which they are in attendance. They may have announcements for public business but they also are responsible for bestowing Awards on members where the people of Insulae Draconis has recommended that a member should be so honoured. Insulae Draconis has many awards to reward people for hard work, skill and courtesy. You can <a href="{% link activities/heraldry/awards.md %}">read more about awards</a>, <a href="http://op.drachenwald.sca.org/"> check whether someone already has an award</a> and then <a href="{% link coronet/recommend.html %}">make an award recommendation</a></p>

  <br />

  <h2>Becoming Prince and Princess</h2>

  <p>An armoured combat Tournament is fought every nine months to determine who will be the Prince and Princess.  Any authorised armoured fighter who is a fully paid member of the SCA and who has the support of a consort (also a fully paid up member) can fight.  The tournament will be officially announced and fighter must give formal notification of their intention to enter with a  <a href="{% link coronet/coronet-tourney.md %}#submit-a-letter-of-intent">Letter of Intent</a>.</p>

  <p class="text-center"><a class="btn btn-primary" href="{% link coronet/past.md %}">Our previous Princes and Princesses</a></p>
                        

</div>
