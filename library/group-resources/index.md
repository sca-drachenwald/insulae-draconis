---
sidebar: sidebar-groups
prevurl: /Library/GroupResources/
title: Resources for Groups in Insulae Draconis
subtitle: Starting up, running a group, running events and reporting up
---

## Starting a Group

- [Introducing Hamlets]({% link library/howtos/hamlets.md %})
- [Some considerations regarding setting up a new group]({% link library/group-resources/how-to-setup.md %})

## Running Events

- [Bidding for Principality or Crown events]({% link library/group-resources/bidding-for-events.md %})
- [Planning and running events, budgeting, etc]({% link library/howtos/running-an-event.md %})

## Group resources

- [How to be a Group Seneschal]({% link library/group-resources/shire-seneschal.md %})
- [How to be a Group Exchequer]({% link library/group-resources/shire-exchequer.md %})
- [How to be a Group Chatelaine]({% link library/group-resources/shire-chatelaine.md %})
- [How to be a Group MoAS]({% link library/group-resources/shire-moas.md %})
- [How to be a Group Captain of Archers]({% link library/group-resources/shire-captain-of-archers.md %})
- [How to be a Group Knight Marshal]({% link activities/armoured-combat/how-to-knight-marshal.md %})
- [Some tips and tricks running an event]({% link library/howtos/running-an-event.md %})
- [Bidding for a Principality event]({% link library/group-resources/bidding-for-events.md %})
- [Some things to think about if you wish to set up a group]({% link library/group-resources/how-to-setup.md %})

- [Find a Principality Officer]({% link governance/find-an-officer.html %})